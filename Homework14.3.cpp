#include <iostream>
#include "stdint.h"
#include <string>
#include <iomanip>

int main()
{
    std::string numbers = "123456789";
    std::cout << numbers << "\n";
    std::cout << "Length = " << numbers.length() << "\n";
    std::cout << "First symbol = " << numbers[0] << "\n";
    std::cout << "Last symbol = " << numbers[numbers.size() -1] << "\n";
    return 0;
}
